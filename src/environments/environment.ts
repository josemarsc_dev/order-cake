// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAzKnDzsyeVSEruARdOQv6r6CvEcdqD01A",
    authDomain: "order-cake-ca670.firebaseapp.com",
    databaseURL: "https://order-cake-ca670.firebaseio.com",
    projectId: "order-cake-ca670",
    storageBucket: "",
    messagingSenderId: "37760588769",
    appId: "1:37760588769:web:f8548ce87d193bcb8250a0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
