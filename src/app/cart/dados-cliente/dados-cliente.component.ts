import { Component, OnInit, Input } from '@angular/core';
import { Client } from 'src/app/model/client.model';
import { CepService } from 'src/app/services/cep/cep.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-dados-cliente',
  templateUrl: './dados-cliente.component.html',
  styleUrls: ['./dados-cliente.component.scss'],
})
export class DadosClienteComponent implements OnInit {
  @Input()
  cliente: Client = { endereco: { bairro: null, cep: null, cidade: null, numero: null, rua: null }, nome: null, telefone: null };

  constructor(private cepService: CepService) { }

  ngOnInit() {

  }

  back() {

  }

  save() {
    console.log(this.cliente);
  }

  focus() {
    let cep = this.cliente.endereco.cep.toString();
    if(cep.length == 8) {
      this.cepService.getAddressByCEP(cep).subscribe(viacep => {
        this.cliente.endereco = {
          cep: viacep['cep'].replace('-', ''),
          cidade: viacep['localidade'],
          bairro: viacep['bairro'],
          rua: viacep['logradouro'],
          numero: undefined,
        }
      });
    }
  }

}
