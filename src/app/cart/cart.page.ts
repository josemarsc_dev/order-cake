import { Component, OnInit } from '@angular/core';
import { Cake } from '../model/cake.model';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { Cart } from '../model/cart.model';
import { StorageService } from '../services/storage/storage.service';
import { Client } from '../model/client.model';
import { CartFirebaseService } from '../services/cart-firebase/cart-firebase.service';

import { DadosClienteComponent } from './dados-cliente/dados-cliente.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  cakesOnCart: Cart = { bolos: [], client: null, data: null };
  total: number = 0;
  cliente: Client = {
    nome: null,
    telefone: null,
    endereco: null
  }

  constructor(private cartFirebaseService: CartFirebaseService, private storageService: StorageService, private alertController: AlertController, private navController: NavController, private loadingController: LoadingController) {
    this.storageService.getCartId().then(cart_id => {
      if (cart_id != undefined) {
        this.cartFirebaseService.getCart(cart_id).subscribe(cart => {
          this.cakesOnCart = cart;
        })
      }
    });
  }

  ngOnInit() {
    this.updateCart();
  }

  updateCart() {
    this.storageService.getCartId().then(cart_id => {
      if (cart_id != undefined) {
        this.cartFirebaseService.getCart(cart_id).subscribe(cart => {
          this.cakesOnCart = cart;
          this.total = 0;
          this.cakesOnCart.bolos.forEach(cake => {
            this.total += cake.cake.preco * cake.qtde;
          })
        })
      }
    });
  }

  removeCart(cake: Cake) {
    console.log(cake);
  }

  async confirmDeleteCart(cake: Cake) {
    const alert = await this.alertController.create({
      header: 'Deseja remover esse item do carrinho?',
      subHeader: cake.nome,
      message: 'Essa operação não pode ser desfeita',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Sim',
          handler: () => {
            this.removeCart(cake);
          }
        }
      ]
    });

    await alert.present();

    this.ngOnInit();
  }
  async confirmEmptyCart() {
    const alert = await this.alertController.create({
      header: 'Tem ceteza?',
      subHeader: 'Que deseja esvaziar o carrinho?',
      message: 'Essa operação não pode ser desfeita',
      buttons: [
        {
          text: 'Voltar',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Esvaziar',
          handler: () => {
            // this.cartService.emptyCart();
            this.updateCart();
          }
        }
      ]
    });

    await alert.present();

    this.ngOnInit();
  }

  goHome() {
    this.navController.pop();
  }

  emptyCart() {
    this.confirmEmptyCart();
  }

  confirmCart() {
    this.promptConfirmCart();
  }

  async promptConfirmCart() {

    this.navController.navigateForward('dados');

    // const alert = await this.alertController.create({
    //   header: 'Dados para entrega',
    //   inputs: [
    //     { name: 'nome', type: 'text', placeholder: 'Seu nome', id: 'nome' },
    //     { name: 'email', type: 'email', placeholder: 'username@domain.com', id: 'email' },
    //     { name: 'bairro', type: 'text', placeholder: 'Bairro', id: 'bairro' },
    //     { name: 'rua', type: 'text', placeholder: 'Rua', id: 'rua' },
    //     { name: 'numero', type: 'number', placeholder: 'numero', id: 'numero' }
    //   ],
    //   buttons: [
    //     {
    //       text: 'Cancelar', role: 'cancel', cssClass: 'secondary',
    //       handler: () => {

    //       }
    //     }, {
    //       text: 'Confirmar',
    //       handler: () => {
    //         // this.presentLoading();
    //       }
    //     }
    //   ]
    // });

    // await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      spinner: 'lines-small',
      message: 'Carregando...',
      duration: 1000
    });
    await loading.present();

    // this.cartService.emptyCart();

    await loading.onDidDismiss().then(whateve => {
      this.presentAlert();
      this.updateCart();
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Confirmado!',
      subHeader: 'Já está a caminho...',
      buttons: ['OK']
    });

    await alert.present();
  }

}
