import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class Cake {
    public nome: string;
    public imagem: string;
    public descricao: string;
    public serve: number;
    public preco: number;
}
