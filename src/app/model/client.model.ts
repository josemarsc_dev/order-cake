import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class Client {
    public nome: string;
    public telefone: string;
    public endereco: {
        cep: string,
        cidade: string,
        bairro: string,
        rua: string,
        numero: number
    }
}
