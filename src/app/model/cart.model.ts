import { Injectable } from '@angular/core';
import { Client } from './client.model';
import { Cake } from './cake.model';

@Injectable({
    providedIn: 'root'
})
export class Cart {
    public data: string;
    public client: Client
    public bolos: { cake: Cake, qtde: number}[];
}
