import { TestBed } from '@angular/core/testing';

import { CartFirebaseService } from './cart-firebase.service';

describe('CartFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CartFirebaseService = TestBed.get(CartFirebaseService);
    expect(service).toBeTruthy();
  });
});
