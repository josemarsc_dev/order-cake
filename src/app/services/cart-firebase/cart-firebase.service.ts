import { Injectable } from '@angular/core';
import { Cart } from 'src/app/model/cart.model';
import { AngularFirestoreCollection, DocumentReference, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { StorageService } from '../storage/storage.service';
import { Cake } from 'src/app/model/cake.model';
import { Client } from 'src/app/model/client.model';

@Injectable({
  providedIn: 'root'
})
export class CartFirebaseService {

  private carts: Observable<Cart[]>;
  private localCart: Cart = { bolos: [], client: null, data: null };
  private cartCollection: AngularFirestoreCollection<Cart>;

  private cart: Cart;
  private client: Client;

  constructor(private storage: StorageService, private angularFireStore: AngularFirestore) {
    this.cartCollection = this.angularFireStore.collection<Cart>('cart');
    this.carts = this.cartCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    this.carts.subscribe((cart) => {
      cart.map(cart => {
        this.localCart = cart;
        if (this.localCart.bolos.length == undefined) {
          this.localCart.bolos = [];
        }
      })
    })
  }

  getCart(id: any): Observable<Cart> {
    return this.cartCollection.doc<Cart>(typeof id == "string" ? id : id.toString()).valueChanges().pipe(
      take(1),
      map((cart) => {
        return cart
      })
    )
  }

  addToCart(cake: Cake, qtd: number): Promise<void | DocumentReference> {
    let increased: boolean = false;

    this.localCart.bolos.forEach(cart => {
      if (cart.cake.nome == cake.nome) {
        cart.qtde += qtd;
        increased = true;
      }
    });

    if (!increased)
      this.localCart.bolos.push({ cake: cake, qtde: qtd });

    return this.storage.getCartId().then((cart_id) => {
      if (cart_id != undefined) {
        return this.angularFireStore.collection('/cart/').doc(cart_id).update(this.localCart).then(res => {
          return res;
        });
      } else {
        this.localCart.client = { endereco: { cep: null, bairro: null, cidade: null, numero: null, rua: null }, nome: null, telefone: null };
        return this.angularFireStore.collection('/cart').add(this.localCart).then(added => {
          return this.storage.saveCartId(added.id);
        })
      }
    });

  }

  deleteCake(id: string): Promise<void> {
    return this.cartCollection.doc(id).delete();
  }
}
