import { Injectable } from '@angular/core';
import { Client } from 'src/app/model/client.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CepService {

  constructor(private http: HttpClient) { }

  getAddressByCEP(cep: string): Observable<Object> {
    return this.http.get("https://viacep.com.br/ws/" + cep + "/json/");
  }
}
