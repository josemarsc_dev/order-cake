import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { Client } from 'src/app/model/client.model';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) {

  }

  saveCartId(_id: String) {
    this.storage.set("cart_id", _id);
  }

  getCartId() {
    return this.storage.get("cart_id");
  }

  removeAll() {
    this.storage.clear();
  }

  public set(settingName, value) {
    return this.storage.set(`setting:${settingName}`, value);
  }
  public async get(settingName) {
    return await this.storage.get(`setting:${settingName}`);
  }
  public async remove(settingName) {
    return await this.storage.remove(`setting:${settingName}`);
  }
}
