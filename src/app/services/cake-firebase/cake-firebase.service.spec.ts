import { TestBed } from '@angular/core/testing';

import { CakeFirebaseService } from './cake-firebase.service';

describe('CakeFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CakeFirebaseService = TestBed.get(CakeFirebaseService);
    expect(service).toBeTruthy();
  });
});
