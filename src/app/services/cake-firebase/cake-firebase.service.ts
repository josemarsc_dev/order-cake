import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Cake } from '../../model/cake.model';

@Injectable({
  providedIn: 'root'
})
export class CakeFirebaseService {

  private cakes: Observable<Cake[]>;
  private cakeCollection: AngularFirestoreCollection<Cake>;

  constructor(private afs: AngularFirestore) {
    this.cakeCollection = this.afs.collection<Cake>('cakes');
    this.cakes = this.cakeCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getCakes(): Observable<Cake[]> {
    return this.cakes;
  }
 
  getCake(id: string): Observable<Cake> {
    return this.cakeCollection.doc<Cake>(id).valueChanges().pipe(
      take(1),
      map(cake => {
        return cake
      })
    );
  }
 
  addCake(cake: Cake): Promise<DocumentReference> {
    return this.cakeCollection.add(cake);
  }
 
  deleteCake(id: string): Promise<void> {
    return this.cakeCollection.doc(id).delete();
  }
}
