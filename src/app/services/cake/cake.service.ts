import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CakeService {
  constructor(private http: HttpClient) { }

  getCakes() {
    return this.http.get('./assets/cakes.json');
  }
}
