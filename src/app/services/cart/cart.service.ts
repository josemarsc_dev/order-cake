import { Injectable } from '@angular/core';
import { Cake } from '../../model/cake.model';
import { Cart } from '../../model/cart.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cakesOnCart: Cart = new Cart();

  constructor() {
    this.cakesOnCart.bolos = Array<{ cake: null, qtde: 0 }>();
  }

  addCake(cake: Cake, qtde: number) {
    let found: boolean = false;
    this.cakesOnCart.bolos.forEach(element => {
      if (element.cake == cake) {
        element.qtde += qtde;
        found = true;
      }
    });
    if (!found)
      this.cakesOnCart.bolos.push({ cake, qtde });
  }

  removeCake(cake: Cake) {
    this.cakesOnCart.bolos.forEach(remCake => {
      if (cake == cake) {
        this.cakesOnCart.bolos.splice(this.cakesOnCart.bolos.indexOf(remCake), 1);
      }
    });
  }

  getCart(): Cart {
    return this.cakesOnCart;
  }

  getPriceCart(): Number {
    let total = 0;
    this.cakesOnCart.bolos.forEach(cake => {
      total += cake.cake.preco * cake.qtde;
    });
    return total;
  }

  emptyCart() {
    this.cakesOnCart.bolos = [];
  }
}
