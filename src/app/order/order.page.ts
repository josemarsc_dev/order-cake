import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Cake } from '../model/cake.model';
import { NavController, AlertController } from '@ionic/angular';

import { Observable } from 'rxjs';
import { CakeFirebaseService } from '../services/cake-firebase/cake-firebase.service';
import { Cart } from '../model/cart.model';
import { CartFirebaseService } from '../services/cart-firebase/cart-firebase.service';
import { StorageService } from '../services/storage/storage.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage {

  cakesOnCart: Cart;
  cakesOnCartFire: Observable<Cart[]>;

  cake: Cake = { nome: null, descricao: null, imagem: null, preco: null, serve: null };
  cakeObservable: Observable<Cake>;

  qtde: number = 1;

  cartPrice: number = 0;

  state: any;

  constructor(private storage: StorageService, private cartFirebase: CartFirebaseService, private cakeFirebaseService: CakeFirebaseService, private router: Router, private navController: NavController, private alertController: AlertController) {
    this.state = this.router.getCurrentNavigation().extras.state;
    if (this.state != undefined) {
      this.cakeFirebaseService.getCake(this.state['cake'].id).subscribe(cake => {
        this.cake = cake;
      });
    } else {
      this.navController.pop();
      this.router.navigateByUrl('/home');
    }

    this.updateCart();
  }

  updateCart() {
    this.storage.getCartId().then(cart_id => {
      if (cart_id != undefined) {
        this.cartFirebase.getCart(cart_id).subscribe(cart => {
          this.cakesOnCart = cart;
          this.cartPrice = 0;
          this.cakesOnCart.bolos.forEach(cake => {
            this.cartPrice += cake.cake.preco * cake.qtde;
          })
        })
      }
    });
  }

  goBack() {
    this.navController.pop();
  }

  addCart(cake: Cake) {
    this.cartFirebase.addToCart(cake, this.qtde).then((res) => {
      this.presentAlert(cake);
    }).catch(err => {
      console.log(err);
    });
    this.updateCart();
  }

  openCart() {
    this.navController.navigateForward(['/cart']);
  }

  async presentAlert(cake: Cake) {
    const alert = await this.alertController.create({
      header: 'Adicionado ao carrinho',
      subHeader: cake.nome,
      message: cake.nome + ' foi adicionado ao carrinho com sucesso',
      buttons: [
        {
          text: 'Ok',
          handler: (qtde) => {
          }
        }
      ]
    });

    await alert.present();

    await this.navController.pop();
  }

  selectOtherQtde() {
    this.otherQtde();
  }

  selectQtde() {
    this.selectQtdeAlert();
  }

  async selectQtdeAlert() {
    const alert = await this.alertController.create({
      header: 'Quantidade',
      subHeader: 'Selecione a quantidade desejada:',
      inputs: [
        { name: 'qtde1', type: 'radio', value: 1, label: '1', checked: this.qtde == 1 },
        { name: 'qtde2', type: 'radio', value: 2, label: '2', checked: this.qtde == 2 },
        { name: 'qtde3', type: 'radio', value: 3, label: '3', checked: this.qtde == 3 },
        { name: 'qtde4', type: 'radio', value: 4, label: '4', checked: this.qtde == 4 },
        {
          name: 'other_qtde',
          type: 'radio',
          value: 0,
          label: 'Outro' + (this.qtde > 4 ? ': ' + this.qtde : ''),
          checked: (this.qtde > 4 ? true : false),
          handler: () => {
            alert.dismiss();
            this.otherQtde();
          }
        }
      ],
      buttons: [
        {
          text: 'ok',
          handler: (qtde) => { this.qtde = qtde; }
        },
        { text: 'Cancelar', role: 'cancel' }
      ]
    });

    await alert.present();
  }

  async otherQtde() {
    const alert = await this.alertController.create({
      header: 'Quantidade',
      subHeader: 'Digite a quantidade desejada:',
      inputs: [
        { name: 'qtde', type: 'number', placeholder: 'Quantidade', value: (this.qtde > 4 ? this.qtde : 5) }
      ],
      buttons: [
        {
          text: 'OK',
          handler: (data) => {
            this.qtde = data.qtde
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });

    await alert.present();
  }

}
