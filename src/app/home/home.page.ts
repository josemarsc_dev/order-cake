import { Component } from '@angular/core';
import { CakeService } from '../services/cake/cake.service'
import { Router, NavigationExtras } from '@angular/router';
import { Cake } from '../model/cake.model';
import { CartService } from '../services/cart/cart.service';
import { NavController, LoadingController } from '@ionic/angular';
import { CakeFirebaseService } from '../services/cake-firebase/cake-firebase.service';
import { Observable } from 'rxjs';

import { StorageService } from '../services/storage/storage.service';
import { Cart } from '../model/cart.model';
import { CartFirebaseService } from '../services/cart-firebase/cart-firebase.service';
import { Client } from '../model/client.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  cakes: Array<Cake>;
  cakesOnCart: Cart;

  cartPrice: number = 0;

  cake: Cake = { nome: null, descricao: null, imagem: null, preco: null, serve: null };
  cart: Cart = { bolos: [], client: null, data: null };
  client: Client = { nome: null, endereco: null, telefone: null };

  loading: Promise<HTMLIonLoadingElement>;

  constructor(private loadingController: LoadingController, private cartFirebase: CartFirebaseService, private storageService: StorageService, private cakeService: CakeService, private router: Router, private cartService: CartService, private navController: NavController, private cakeFirebase: CakeFirebaseService) {
    // this.storageService.removeAll();

    this.loading = this.showLoading();
    this.loading.then(e => {
      e.present();
    });

    this.cakeFirebase.getCakes().subscribe((cakes) => {
      this.cakes = cakes;
      if (this.cakes.length != 0) {
        this.loading.then((e) => {
          e.dismiss();
        })
      }
    });
  }

  ionViewDidEnter() {
    this.storageService.getCartId().then(cart_id => {
      if (cart_id != undefined) {
        this.cartFirebase.getCart(cart_id).subscribe(cart => {
          this.cakesOnCart = cart;
          this.cartPrice = 0;
          this.cakesOnCart.bolos.forEach(cake => {
            this.cartPrice += cake.cake.preco * cake.qtde;
          })
        })
      }
    });
  }

  openCart() {
    this.navController.navigateForward('/cart');
  }

  order(cake: Cake) {
    let extras: NavigationExtras = {
      state: {
        cake: cake
      }
    };
    this.navController.navigateForward('/order', extras);
  }

  async showLoading() {
    return await this.loadingController.create({
      spinner: 'lines-small',
      message: 'Carregando dados. Aguarde...'
    });
  }

}
